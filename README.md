1.
timedatectl set-timezone Europe/Paris
hwclock -w
timedatectl status
2.
apt update -y && apt upgrade -y
3.
apt install -y sudo
4.
sudo apt install -y git make apt-transport-https ca-certificates curl
5.
sudo swapoff -a && sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
6.
wget https://go.dev/dl/go1.21.3.linux-amd64.tar.gz
7.
sudo rm -rf /usr/local/go
8.
sudo tar -C /usr/local -xzf go1.21.3.linux-amd64.tar.gz
9.
export PATH=$PATH:/usr/local/go/bin
10.
git clone https://github.com/Mirantis/cri-dockerd.git
11.
# Build
cd cri-dockerd
make cri-dockerd
 
# Installation
sudo mkdir -p /usr/local/bin
sudo install -o root -g root -m 0755 cri-dockerd /usr/local/bin/cri-dockerd
12.
sudo install packaging/systemd/* /etc/systemd/system
sudo sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
sudo systemctl daemon-reload
sudo systemctl enable --now cri-docker.socket
13.

sudo mkdir -m 755 -p /etc/apt/keyrings
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
14.
sudo apt update -y
sudo apt install -y kubelet kubeadm kubectl
sudo apt hold kubelet kubeadm kubectl
15.

# Pour tous les utilisateurs
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null
 
# Pour l'utilisateur en cours uniquement
# echo "source <(kubectl completion bash)" >> ~/.bashrc
 
# Pour un utilisateur spécifique uniquement
# echo "source <(kubectl completion bash)" >> /home/user/.bashrc
16.
sudo kubeadm init \
        --pod-network-cidr=10.244.0.0/16 \
        --cri-socket unix:///var/run/cri-dockerd.sock
17.
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
18.
sysctl net.bridge.bridge-nf-call-iptables=1
 
kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
 
# si nécessaire
kubectl edit cm -n kube-system kube-flannel-cfg
# edit network 10.244.0.0/16 to 10.10.0.0/16 pour dashboard
19.
kubeadm join 192.168.1.80:6443 --token dtjqeb.oidbk4xll8sswj0i \
        --discovery-token-ca-cert-hash sha256:0559dcf4cedb5be68aa8b825c6c47d7cd6db133c81ef638649ae87be05f2cb29 \
        --cri-socket unix:///var/run/cri-dockerd.sock
20.
export KUBECONFIG=/etc/kubernetes/admin.conf
#export $HOME/.kube/config
# De façon permanente
# echo 'export KUBECONFIG=$HOME/admin.conf' >> $HOME/.bashrc

